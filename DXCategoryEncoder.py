#!/usr/bin/env python
# encoding: utf-8
'''
@author: huzhehui
@mail: hzhmayday@gmail.com
'''
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from tqdm import tqdm


class DXCategoryEncoder(BaseEstimator, TransformerMixin):

    def __init__(self, ):
        self.OutputColumns = []             # 最终处理完后的columns

    def fit(self, X, y=None):
        return self

    def transform(self, X, ):
        for col in tqdm(X.columns):
            X[col] = X[col].apply(lambda x: x if type(x)==str and x.strip()!="" else np.nan)
        # 输出最终的columns
        self.OutputColumns = X.columns

        return X


if __name__=="__main__":
    x = DXCategoryEncoder()
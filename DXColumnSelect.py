#!/usr/bin/env python
# encoding: utf-8
'''
@author: huzhehui
@mail: hzhmayday@gmail.com
'''
from sklearn.base import BaseEstimator, TransformerMixin

class DXColumnSelect(BaseEstimator,TransformerMixin):
    '''
    选择目标的数据类型
    '''
    ContinuousColumns = ['R6_CDMA_CALL', 'R6_CDMA_FLOW', 'R6_DS_ROAM_VOL_LIST', 'T0_ACT_DAYS', 'T0_DS_VOL', 'ZIXUN_CNT',
                         'R6_BIL_SMS_AMT_LIST', 'N_CCUST_BRD_OCS_STMT', 'N_CCUST_CDMA_OCS_STMT', 'N_CCUST_FIX_OCS_STMT',
                         'N_CCUST_ITV_OCS_STMT', 'N_CCUST_OCS_STMT', 'HG_ACTIVE_DAYS', 'CALL_CNT', 'R3_SMS_FIN_AMT',
                         'R3_CCUST_OCS_STMT', 'T0_QQW_ACT_DAYS', 'T0_MKT_STAR_GRADE_NAME', 'SMS_CNT',
                         'R6_BIL_SYS_AMT_LIST', 'BIL_SYS_AMT', 'BIL_VAS_AMT', 'BIL_VS_AMT', 'R3_CCUST_BRD_OCS_STMT',
                         'R3_CCUST_CDMA_OCS_STMT', 'R3_CCUST_FIX_OCS_STMT', 'R3_CCUST_ITV_OCS_STMT', 'R3_FIN_AMT',
                         'R3_VS_FIN_AMT', 'CMPLN_CNT', 'T0_CALL_DUR',
                         'R3_CCUST_SINGLE_FIN_AMT', 'DS_OVER_AMT', 'T0_OWE_AMT', 'OWE_MON_CNT', 'PROM_AVG_DS_VOL',
                         'T0_LLB_AMT', 'T0_FREE_CALL_USE_LV', 'N_CACCT_BRD_OCS_STMT', 'R3_ACT_DAYS', 'T0_FREE_DS_LIMIT',
                         'R3_CACCT_BRD_OCS_STMT', 'T0_FREE_CALL_LIMIT', 'R3_CALL_CNT', 'R3_CALL_DUR',
                         'T0_VOLTE_ACT_DAYS', 'R3_DS_VOL', 'R3_SMS_CNT', 'T0_QQW_DUR', 'R6_MKT_CONTACT_CNT',
                         'MKT_CONTACT_CNT', 'R6_UPB_MONTH', 'R3_DS_OVER_AMT', 'R3_LLB_AMT', 'T0_O_CTC_CALL_DUR',
                         'T0_NIGHT_VOL', 'PROM_T0_2_FIN_AMT', 'PROM_T0_1_FIN_AMT', 'PROM_T0_FIN_AMT',
                         'T0_MULTI_DS_USE_LV', 'R3_DS_OVER_NBD_AMT', 'T0_1_FREE_CALL_LIMIT', 'T0_2_FREE_CALL_LIMIT',
                         'R3_VS_OVER_NBD_AMT', 'CUST_R3_DS_VOL', 'PROM_R3_DS_VOL', 'CUST_T0_DS_VOL', 'PROM_T0_DS_VOL',
                         'R3_VS_USE_LV', 'CDMA_T0_1_VS_DUR', 'CDMA_T0_2_VS_DUR', 'CDMA_T0_1_DS_VOL', 'N_NET_FLOW',
                         'PROM_T0_1_DS_VOL', 'PROM_T0_2_DS_VOL', 'PROM_T0_2_DS_OVER_AMT', 'PROM_T0_VS_DUR',
                         'PROM_T0_1_VS_DUR', 'PROM_T0_2_VS_DUR', 'PROM_T0_1_DS_OVER_AMT', 'PROM_T0_DS_OVER_AMT',
                         'PROM_T0_2_VS_OVER_AMT', 'PROM_T0_1_VS_OVER_AMT', 'PROM_T0_VS_OVER_AMT', 'CDMA_T0_2_DS_VOL',
                         'T0_2_MULTI_DS_LIMIT', 'T0_1_MULTI_DS_LIMIT', 'R3_MULTI_DS_USE_LV', 'R3_PROM_BIL_DS_AMT',
                         'CUST_T0_NASSET_FIN_AMT', 'T0_T_CALL_CNT',
                         'T0_FIN_AMT', 'LAN_T0_ACT_DAYS', 'LAN_T0_1_DS_VOL', 'LAN_T0_1_ACT_DAYS', 'LAN_T0_2_DS_VOL',
                         'LAN_T0_2_ACT_DAYS', 'SPEED', 'CACCT_BRD_EFF_DT',
                         'PROM_ACT_LATN_CNT', 'PROM_BRD_CNT', 'PROM_BRDSPEED_UNMATCH_FLG',
                         'BRD_SPEED_VALUE_UP_TARGET_FLG', 'LAN_CHARGE_UP_AMT', 'CDSC_MON_AMT',
                         'CUST_BRD_MIX_C_CNT', 'CACCT_BEN_BAL_AMT', 'PROM_CHANGE_DT', 'QQW_CNT', 'QQW_FLG',
                         'SERV_MONTH', 'ORDER_VOLTE_FLG', 'CDMA_4G_FLG', 'PROM_C_FK_CNT', 'CCUST_C_ZK_CNT',
                         'CCUST_FIX_CNT', 'CCUST_ITV_CNT', 'LST_SRV_VAL', 'MKT_STAR_GRADE_NAME',
                         'POINT_TYPE_D_VAL', 'BXL_LLB_FLG', 'PROM_AMT', 'QQW_PROM_FLG',
                         'PROM_AGREE_EXP_TYPE', 'INTEL_ROAM_FLU_FLG', 'TYSX_USER_FLG', 'YZF_AGREEMENT_PAS_FLG',
                         'YZF_ORL_FLG', 'ORDER_YZF_TC_FLG', 'FREE_CHARGE_FLG', 'RECHARGE_CNT', 'BASE_POINT_NEW', 'AGE',
                         'CCUST_BRD_CNT', 'CCUST_C_CNT', 'CCUST_C_FK_CNT', 'FREE_DS_USED_P', 'FOLLOW_FLG',
                         'CUR_BXL_DS_USED', 'CCUST_WT_BIND_FLG', 'CDMA_5G_FLG', 'LAST_MKT_CONTACT_DT', 'ASSET_5G_FLG',
                         'PROM_FK_USABLE_LIMIT_CNT', 'MNP_SEL_DATE', 'SERV_DAYS', 'FK_ACT_CNT',
                         'PROM_ACT_NBR_CNT', 'CUST_30ACT_NBR_CNT', 'T0_MULTI_DS_LIMIT', 'PROM_30ACT_NBR_CNT',
                         'PROM_NBR_CNT', 'CCUST_ELSE_PROM_C_CNT', 'CCUST_ELSE_PROM_BRD_CNT', 'CCUST_ELSE_PROM_ITV_CNT',
                         'LAST_SCORE_USE_DATE', 'PROM_LLB_AMT', 'PROM_NO_BIND_PROM_FK_CNT', 'CDSC_EXP_DAY',
                         'ASSET_5G_VOL_FLG', 'CUST_INV_CDMA_CNT', 'HIST_PRODUCT_UPB_5G_FLG', 'CUST_UNACT_FK_CNT',
                         'CUST_T0_FIN_AMT', 'CUR_DS_VOL', 'MON_CACCT_BJ_BAL_AMT',
                         'LAST30DAYS_RECHARGE_FLG', 'VIDEO_CRBT_5G_FLG', 'CUR_MULTI_DS_OVER', 'BASE_POINT',
                         'CDMA_AGREE_FLG', 'CUST_LAST90DAYS_ASSET_OUT_FLG', 'CUR_CALL_DUR', 'CUR_SMS_CNT',
                         'PROM_5G_FLG', 'PROM_FREE_FK_USABLE_CNT', 'PROM_CHARGE_FK_AMT']
    CategoricalColumns = ["city", "TER_PERFER", "GENDER", "LAST_MKT_CONTACT_CHANNEL", "PROM_TYPE", "MAIN_PROM_TYPE",
                          "LAN_CHARGE_UP_FLG", 'CDSC_TYPE_NAME', 'CCUST_PRD_TYPE', 'PROM_PRD_TYPE','CACCT_BRD_CDSC_TYPE_NAME']

    def __init__(self,columns_type:str):
        self.columns_type = columns_type
        self.OutputColumns = []    # 筛选完后的columns

    def fit(self,X,y=None):
        '''
        将X分割成3个部分，分别是 数据库中的分类、非分类和非数据库
        ---------
        :param X: pd.DataFrame
        :param y:
        :return: DXColumnSelect
        '''
        if self.columns_type == "category":
            attribute_names = DXColumnSelect.CategoricalColumns
        elif self.columns_type == "constant":
            attribute_names = DXColumnSelect.ContinuousColumns
        elif self.columns_type == "leave":
            attribute_names = list( set(X.columns)-set(DXColumnSelect.CategoricalColumns)-set(DXColumnSelect.ContinuousColumns) )
        else:
            raise NameError("参数写错...")
        self.OutputColumns = [i for i in X.columns if i in attribute_names]
        return self


    def transform(self,X):
        return X[self.OutputColumns]


if __name__ == "__main__":
    print(DXColumnSelect.ContinuousColumns)
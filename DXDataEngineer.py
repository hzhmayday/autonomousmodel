#!/usr/bin/env python
# encoding: utf-8
'''
@author: huzhehui
@mail: hzhmayday@gmail.com
'''
from sklearn.preprocessing import OrdinalEncoder
from sklearn.pipeline import Pipeline, FeatureUnion
from china_tel.AutonomousModel.DXColumnSelect import DXColumnSelect
from china_tel.AutonomousModel.DXFeatureEncoder import DXFeatureEncoder
from china_tel.AutonomousModel.DXCategoryEncoder import DXCategoryEncoder
from sklearn.impute import SimpleImputer
from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
import numpy as np

class DXDataEngineer(BaseEstimator, TransformerMixin):
    def __init__(self, date=None):
        self.date =date
        self.leaveColumns = DXColumnSelect(columns_type="leave")    # 查看X中数据库外的columns

        self.Categorical_pipeline = Pipeline([("select_col", DXColumnSelect(columns_type="category")),
                                             ("category_encoder", DXCategoryEncoder()),
                                             ("fillna", SimpleImputer(strategy="constant", fill_value="-9999999")),
                                             ("labelencoder", OrdinalEncoder()),
                                             ])

        self.Continuous_pipeline = Pipeline([("select_col", DXColumnSelect(columns_type="constant")),
                                            ("feature_encoder", DXFeatureEncoder(date=self.date)),
                                            ("fillna", SimpleImputer(strategy="constant", fill_value=-9999999)),
                                            ])
        # 训练合并的FeatureUnion
        self.all_pipeline = FeatureUnion(transformer_list=[("categorical", self.Categorical_pipeline),
                                                           ("continuous", self.Continuous_pipeline)])

    def fit(self, X, y=None):
        return self


    def transform(self, X, y=None):
        self.leaveColumns.fit_transform(X,)
        output = self.all_pipeline.fit_transform(X, )
        Categorical_columns = self.Categorical_pipeline.named_steps["select_col"].OutputColumns
        Continuous_columns = self.Continuous_pipeline.named_steps["feature_encoder"].OutputColumns
        output_df = pd.DataFrame(output, columns=list(Categorical_columns)+list(Continuous_columns))
        return output_df.applymap(np.double)
#!/usr/bin/env python
# encoding: utf-8
'''
@author: huzhehui
@mail: hzhmayday@gmail.com
'''
import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from tqdm import tqdm


class DXFeatureEncoder(BaseEstimator, TransformerMixin):

    def __init__(self, date:str):
        self.date = date                    # 时间特征处理的date
        self.OutputColumns = []             # 最终处理完后的columns


    def columns_deal(self, X, col:str) -> None:
        # 六月合并类型 —— 情况一
        if col in ("R6_CDMA_CALL","R6_CDMA_FLOW",):
            R6 = X[col].\
                 apply(lambda x: [np.double(i.split("&")[1]) for i in x.split("||")] if type(x)==str and x not in (None,"") else [-1]*6).\
                 apply(lambda x: (x+(6-len(x))*[-1]) if len(x)<6 else x)
            R6_son_columnns = "RR{}"+"_".join(col.split("_")[1:])   # 拆分出来的columns名称
            R6_son_columnns_DF = pd.DataFrame(list(R6), columns=[R6_son_columnns.format(i) for i in np.arange(1,7,1)])
            # 循环在原始X上添加新增的列
            for R6_col in [R6_son_columnns.format(i) for i in np.arange(1,7,1)]:
                X[R6_col] = R6_son_columnns_DF[R6_col]
            X.drop(col, axis=1, inplace=True)

        # 六月合并类型 —— 情况二
        elif col in ("R6_BIL_SMS_AMT_LIST","R6_BIL_SYS_AMT_LIST","R6_DS_ROAM_VOL_LIST",):
            R6 = X[col]. \
                apply(lambda x: [np.double(i.split("&")[1][:-1]) for i in x.split("||")] if type(x) == str and x not in (None, "") else [-1] * 6). \
                apply(lambda x: (x + (6 - len(x)) * [-1]) if len(x) < 6 else x)
            R6_son_columnns = "RR{}" + "_".join(col.split("_")[1:])  # 拆分出来的columns名称
            R6_son_columnns_DF = pd.DataFrame(list(R6), columns=[R6_son_columnns.format(i) for i in np.arange(1, 7, 1)])
            # 循环在原始X上添加新增的列
            for R6_col in [R6_son_columnns.format(i) for i in np.arange(1, 7, 1)]:
                X[R6_col] = R6_son_columnns_DF[R6_col]
            X.drop(col, axis=1, inplace=True)


        # 时间系列类型
        elif col in ("PROM_CHANGE_DT", "LAST_MKT_CONTACT_DT", "MNP_SEL_DATE", "LAST_SCORE_USE_DATE", "CACCT_BRD_EFF_DT"):
           X[col] = (pd.to_datetime(self.date+"01")+pd.DateOffset(months=1)-pd.DateOffset(days=1) - pd.to_datetime(X[col],errors = 'coerce')).apply(lambda x: x.days)

        # 产品类型数量
        elif col in ("CCUST_PRD_TYPE","PROM_PRD_TYPE"):
            X.loc[X[col].isnull()==False, col] = X.loc[X[col].isnull()==False, col].apply(lambda x: len(x.split("||")))

        #========================
        # 添加额外字段的补充方式
        #========================

        else:
            X[col] = np.double(X[col])


    def fit(self, X, y=None):
        return self


    def transform(self, X, ):
        for col in tqdm(X.columns):
            # print(col)
            # 如果类型是数字类型，且不缺失值就不处理
            if X[col].isnull().sum()==0 and X[col].dtype in (np.float64, np.float32, np.float16, np.int64, np.int32, np.int16, np.int8):
                continue
            self.columns_deal(X, col)

        # 输出最终的columns
        self.OutputColumns = X.columns

        return X


if __name__=="__main__":
    x = DXFeatureEncoder("202104")



